#
#  DESCRIPTOR MODULE FOR VASP, AUTHOR: CHANG LIU
#
#  DEVELOPED IN 2021
#
#################################################
import numpy as np
from ase.io import read, write
from ase.calculators.vasp import VaspChargeDensity as VCD
from ase.units import Bohr
import os

def local_electron_affinity_band(kpt_weight,band_eigenvalue,band_density,E_0):
    """Takes, for a given point in space, the necessary parameters to calculate
    a bands contribution to E(r) * rho(r) for an arbitrary kpoint, unscaled!
    kpt_weight = kpoint weight
    band_eigenvalue = KS-eigenvalue for band at given kpoint
    band_density = local electron density for band at kpoint 
    E_0 = correction factor for referens potential (i.e. potential in vacuum-center) 
    """
    lea = kpt_weight * (band_eigenvalue - E_0) * band_density

class KSDescriptor:
    def __init__(self,calc,**kwargs):
        self.calc = calc
        locpot = VCD('LOCPOT')
        vol = locpot.atoms[0].get_volume()
        v_matrix = locpot.chg[0]*vol
        self.v_matrix = v_matrix
        planar = v_matrix.mean(axis=(0,1)) # average over x and y
        self.E_0 = np.max(planar)

        nkpts = len(calc.get_ibz_k_points())
        self.nkpts = nkpts

        # find number of occupied orbitals, if no fermi smearing
        self.nocc = []
        if calc.get_spin_polarized() == 0:
            self.nocc.append(calc.get_number_of_electrons() // 2)
        else:
            for spin in range(2):
                noc = 0.0
                for kpt in range(nkpts):
                    noc += sum(calc.get_occupation_numbers(kpt=kpt, spin=spin))
                noc = int(noc + 0.5)
                parprint('spin:', spin, 'nocc', noc)
                self.nocc.append(noc)
        parprint('number of valence electrons:', calc.get_number_of_electrons())

    def electrostatic_potential(self):
        return self.v_matrix

    def reference_potential(self):
        return self.E_0

    def parchg_density(self,band = 0, kpt = 0, spin = 0):
        self.calc.set(lpard = True,
                      lsepb = True,
                      lsepk = True,
                      kpuse = kpt,
                      iband = band
        )
        self.calc.calculate()
        parchg = VCD('PARCHG.%04d.%04d'%(band,kpt))
        psum = parchg.chg[0]*Bohr**3
        os.remove('PARCHG.%04d.%04d'%(band,kpt))
        if self.calc.get_spin_polarized() == 0:
            return psum / 2
        else:
            pdiff = parchg.chgdiff[0]*Bohr**3
            if spin == 0:
                return (psum + pdiff) / 2
            elif spin == 1:
                return (psum - pdiff) / 2
    
    def get_electron_density(self):
        chgcar = VCD('CHGCAR')
        dens = chgcar.chg[0]*Bohr**3
        return dens

    def density_descriptor(self,mode,e_r = None):
        #use: mode = 'AIP' or 'MEA'
        #e_r: the eigen energy range of orbitals included, i.e. e_fermi +/- e_r
        implemented_modes = ['AIP','MEA']
        if mode not in implemented_modes:
            raise RuntimeError("Use either 'AIP' or 'MEA'!")
        parprint("Mode: %s"%mode)
  
        E_0 = self.reference_potential()
        dens = self.get_electron_density()
        desc_mat = np.zeros_like(dens)
        nspins = self.calc.get_spin_polarized() + 1
        weights = self.calc.get_k_point_weights()
        for spin in range(nspins):
            for kpt in range(self.nkpts):
                eigen_energies = self.calc.get_eigenvalues(kpt = kpt,spin = spin)
                n_occ = self.nocc[spin]
                e_fermi = (eigen_energies[n_occ-1] + eigen_energies[n_occ]) / 2 
                #I(r)/AIP, for occupied states
                if mode == 'AIP':
                    n_end = n_occ
                    if e_r == None:
                        n_start = 0
                        n = n_occ
                    else:
                        mask = (eigen_energies >= e_fermi - e_r)
                        eps_v = eigen_energies[mask]
                        n_start = len(eigen_energies) - len(eps_v)
                        n = len(eps_v)
                #E(r)/MEA, for unoccupied states
                if mode == 'MEA':
                    v_ref = self.reference_potential()
                    n_start = n_occ
                    mask = (eigen_energies < v_ref)
                    if e_r != None:
                        mask = mask & (eigen_energies <= e_fermi + e_r)
                    eps_v = eigen_energies[mask]
                    n_end = len(eps_v)
                    n = n_end - n_start
                    if n <= 0:
                        raise RuntimeError("Reference potential below Fermi level!")
                parprint("Start: %d; End: %d; Number: %d"%(n_start,n_end-1,n))
                #list of band density
                for band in range(n_start,n_end):
                    parchg_band = self.parchg_density(band=band,kpt = kpt,spin=spin)
                    desc_band = local_electron_affinity_band(weights[kpt],eigen_energies[band],parchg_band,E_0)
                    desc_mat += desc_band
                parprint('kpt:%d done'%kpt)
        desc_mat = np.divide(desc_mat,dens,out=np.zeros_like(desc_mat),where=dens!=0)
        if mode == 'AIP':
            desc_mat = -desc_mat
        #counting both spin systems also for spin-paired cases!
        if nspins == 1:
            desc_mat *= 2
        return desc_mat
